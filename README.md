# uttrekk

[![PyPI - Version](https://img.shields.io/pypi/v/uttrekk.svg)](https://pypi.org/project/uttrekk)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/uttrekk.svg)](https://pypi.org/project/uttrekk)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install uttrekk
```

## Resources

- [Different variants of writting XML in Python](https://gist.github.com/roskakori/3067859)


### Getting started with Ubuntu 22.04 in WSL and Oracle Developer Day in VirtualBox

Download and install [Oracle Developer Day](https://www.oracle.com/database/technologies/databaseappdev-vm.html) in VirtualBox. Se [this tutorial](https://www.thatjeffsmith.com/archive/2014/02/introducing-the-otn-developer-day-database-12c-virtualbox-image/).

Set ut a shared folder with the dump file exported using Oracle Data Pump.

If you use WSL as a development environment, change the networking options for the Developer Day virtual machine to bridged adapter. Get its IP address by running `ip show addr` (in the virtual machine).

Connect to database:

```bash
sqlplus system/oracle@localhost:1521/orcl
```

Find the `DATA_PUMP_DIR`:

```sql
select directory_path from all_directories where directory_name = 'DATA_PUMP_DIR';
```

Connect to the database again (as above) and create the neccesary schema (user) and tablespaces:

```sql
create user UIB_TEST identified by oracle;
create tablespace uib_data datafile 'uib_data.dbf' size 1g;
create tablespace uib_index datafile 'uib_index.dbf' size 1g;
```

Import dump file with Data Pump:

`TODO:` Log file.

```bash
impdp system/oracle@localhost:1521/orcl
```

Now you should be able to connect with dsn `hostname/orcl'.

If the virtual machine at some point loses its IPv4 address, renew the lease:

```bash
sudo dhclient -r
sudo dhclient -4 eth0
```


## License

`uttrekk` is distributed under the terms of the [GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html) license.
