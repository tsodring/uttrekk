# Copyright (C) 2023 Per Christian Gaustad <pcgaustad@mailbox.org>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import hatch_cfg.les_config
import uttrekk.xmlwriter as xw
import decouple
import oracledb


config, logging = hatch_cfg.les_config.last_inn_konfigurasjon_og_logging()

env = 'TEST'

ora_user = decouple.config('ORA_' + env + '_USER')
ora_password = decouple.config('ORA_' + env + '_PASSWORD')
ora_dsn = decouple.config('ORA_' + env + '_DSN')

# TODO: Write real tests

if __name__ == '__main__':
    with oracledb.connect(user=ora_user, password=ora_password,
                          dsn=ora_dsn) as c:
        sw = xw.ArkivstrukturWriter(file=open('tests/tmp/ora_test.xml', 'wb'),
                                    ora_connection=c)
        sw.start_doc()

        sw.get_objects()

        sw.end_doc()
        sw.close()
