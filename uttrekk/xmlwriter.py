# Copyright (C) 2023 Per Christian Gaustad <pcgaustad@mailbox.org>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


from xml.sax import saxutils
import logging
import uttrekk.sql_queries as sql


class SAXWriter():
    def __init__(self, file):
        self.file = file
        self.xml_gen = saxutils.XMLGenerator(file, encoding='utf-8',
                                             short_empty_elements=True)

    def close(self):
        self.file.close()

    def start_doc(self):
        self.xml_gen.startDocument()

    def end_doc(self):
        self.xml_gen.endDocument()

    def start_element(self, name: str, attr: dict = {}):
        self.xml_gen.startElement(name, attr)

    def end_element(self, name: str):
        self.xml_gen.endElement(name)

    def simple_element(self, name: str, contents: str, attr: dict = {}):
        self.start_element(name, attr)
        self.xml_gen.characters(self.esc(contents))
        self.end_element(name)

    def qa(self, s):
        return saxutils.quoteattr(str(s))

    def esc(self, s):
        return saxutils.escape(str(s))


class ArkivstrukturWriter(SAXWriter):
    """Generate arkivstruktur.xml.

    TODO: Add parameter for arkiv/arkivdel.
    TODO: Add more objects than arkiv and arkivdel.
    TODO: Add support for arkivskaper (also if absent).
    TODO: Close cursors (or whole db connection).
    TODO: Quoting of values with saxutils necessary?
    """
    def __init__(self, file, ora_connection):
        c = ora_connection
        self.namespaces = {
            'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            'xmlns': ('http://www.arkivverket.no/standarder/noark5/'
                      'arkivstruktur'),
            'xmlns:n5mdk': ('http://www.arkivverket.no/standarder/noark5/'
                            'metadatakatalog')
        }

        # Supported objects and the link to their parent object
        self.object_hierarchy = [('arkiv', None), ('arkivdel', 'refArkiv')]

        cursors = [
            c.cursor(),
            c.cursor(),
        ]
        self.cursor_execute = [
            cursors[0].execute(sql.select_arkiv),
            cursors[1].execute(sql.select_arkivdel),
        ]
        self.cursor_fetchmany = [
            self.cursor_execute[0].fetchmany(),
            self.cursor_execute[1].fetchmany(),
        ]
        cursor_col_names = [
            tuple(col[0] for col in self.cursor_execute[0].description),
            tuple(col[0] for col in self.cursor_execute[1].description),
        ]
        self.rows_list = [
            sql.rows_to_dicts(cursor_col_names[0], self.cursor_fetchmany[0]),
            sql.rows_to_dicts(cursor_col_names[1], self.cursor_fetchmany[1]),
        ]
        super().__init__(file)

    def traverse_objects(self, obj_rank, parent_systemid=''):
        logging.debug(f'{obj_rank=}')
        obj_name = self.object_hierarchy[obj_rank][0]
        for idx, i in enumerate(self.rows_list[obj_rank]):
            if obj_rank > 0:
                parent_ref = i[self.object_hierarchy[obj_rank][1]]
            # Condition only works if arkiv_cursor.fetchmany() is called only
            # once.
            if obj_name == 'arkiv' and idx == 0:
                self.start_element('arkiv', self.namespaces)
            else:
                self.start_element(obj_name)

            # k is the element name, v its contents
            for k, v in i.items():
                if k != self.object_hierarchy[obj_rank][1]:
                    if not parent_systemid or parent_systemid == parent_ref:
                        if v:
                            self.simple_element(k, v)
                        else:
                            self.simple_element(k, '')
            if obj_rank+1 < len(self.object_hierarchy):
                self.traverse_objects(obj_rank+1, i['systemID'])
            self.end_element(obj_name)

    def get_objects(self, obj_rank=0):
        self.traverse_objects(obj_rank)
