# Copyright (C) 2023 Per Christian Gaustad <pcgaustad@mailbox.org>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import datetime


# Get rid of flake8 warning.
if datetime:
    pass


def to_guid(systemid):
    """Convert raw SYSTEMID to guid."""
    return (
        f"lower(regexp_replace(rawtohex({systemid}), "
        r"'(.{8})(.{4})(.{4})(.{4})(.{12})', '\1-\2-\3-\4-\5'))"
    )


def to_navn(pn_id):
    """Convert PN_ID to navn."""
    return f'(select pn_navn from pernavn where pn_id = {pn_id})'


def rows_to_dicts(cols, rows):
    rows_list = []
    for r in rows:
        row = {}
        for idx, c in enumerate(cols):
            row[c] = r[idx]
        rows_list.append(row)
    return rows_list


select_arkiv = f'''
select
    {to_guid('ar_systemid')} "systemID"
    ,ar_betegn "tittel"
    ,ar_opprettetdato "opprettetDato"
    ,{to_navn('ar_opprettetav_pn')} "opprettetAv"
    ,ar_avsluttetdato "avsluttetDato"
    ,{to_navn('ar_avsluttetav_pn')} "avsluttetAv"
from arkiv
'''

select_arkivdel = f'''
select
    {to_guid('ar_systemid')} "refArkiv"
    ,{to_guid('ad_systemid')} "systemID"
    ,ad_betegn "tittel"
    ,ad_astatus_as "arkivdelstatus"
    ,ad_opprettetdato "opprettetDato"
    ,{to_navn('ad_opprettetav_pn')} "opprettetAv"
    ,ad_avsluttetdato "avsluttetDato"
    ,{to_navn('ad_avsluttetav_pn')} "avsluttetAv"
from arkivdel
inner join arkiv on ad_arkiv_ar = ar_arkiv
'''
